package com.example.k2972.ndk;

import android.app.Activity;
import android.os.Bundle;
import java.util.Random;
import android.widget.TextView;

import java.sql.Time;

public class AlgorythmActivity extends Activity {

    TextView java1, java2, java3;
    int[] inputList = new int[10000];
    int[] data = new int[10000];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_algorythm);
        java1 = (TextView)findViewById(R.id.java1);
        java2 = (TextView)findViewById(R.id.java2);
        java3 = (TextView)findViewById(R.id.java3);
        Random random = new Random();
        String stringend = "Time = ";

        /* TODO: Laita tulokset omiin tekstikenttiin
            Javalle on: java1, java2 ja java3
            C++: cpp1, cpp2 ja cpp3
        */


        for (int i = 0; i < data.length; i++){

            inputList[i] = random.nextInt(10000);

            stringend += inputList[i] + " ";
        }

        data = inputList;

        java1.setText(stringend);
        RunAlgos(3);
    }

    //TODO: Ajastimet
    private void RunAlgos(int times) {

        int[] textViews = {R.id.java1, R.id.java2, R.id.java3};
        int[] textViewsCpp = {R.id.cpp1, R.id.cpp2, R.id.cpp3};

        for (int i=0; i < times; i++) {

            long startTime = System.nanoTime();
            RunJava();
            long endTime =  System.nanoTime();
            long duration = (endTime - startTime) / 1000000;
            TextView tv =(TextView) findViewById(textViews[i]);
            tv.setText(String.valueOf(duration));


            // start time
            long startTimeCpp = System.nanoTime();
             RunCpp(inputList);
            long endTimeCpp =  System.nanoTime();
            long durationCpp = (endTimeCpp - startTimeCpp) / 1000000;
            TextView tvCpp =(TextView) findViewById(textViewsCpp[i]);
            tvCpp.setText(String.valueOf(durationCpp));
            //end time
            // cpp time = endtime-starttime

        }
    }


    private void RunJava (){
        int min;



        for (int i = 0; i < data.length - 1; i++){

            min = i;

            for (int  j = i + 1; j < data.length; j++){

                if (data[j] < data[min]){

                    min = j;

                }

            }

            if (i != min){

                int tmp = data[i];

                data[i] = data[min];

                data[min] = tmp;

            }

        }


    }

    // Funktio löytyy native-lib.cpp
    public native void RunCpp(int[] input);

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }
}
