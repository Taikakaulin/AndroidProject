package com.example.k2972.ndk;

import android.app.Activity;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.ToggleButton;

public class AudioActivity extends Activity {
    // TODO: Audio
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio);

        ToggleButton toggle = (ToggleButton) findViewById(R.id.toggleButton);
        toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
             // The toggle is enabled
            // Ääni päälle
            StartAudio();
        } else {
             // The toggle is disabled
            // Ääni pois päältä
            StopAudio();
         }
        }
        });
    }


    public native String StartAudio();
    public native String StopAudio();

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }
}
