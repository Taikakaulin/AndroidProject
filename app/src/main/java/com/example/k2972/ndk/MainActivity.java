package com.example.k2972.ndk;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity {

    public void openAlgo(View view) {
        Intent myIntent = new Intent(MainActivity.this, AlgorythmActivity.class);
        MainActivity.this.startActivity(myIntent);

    }

    public void openAudio(View view) {
        Intent myIntent = new Intent(MainActivity.this, AlgorythmActivity.class);
        MainActivity.this.startActivity(myIntent);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    // Example of a call to a native method
    //TextView tv = (TextView) findViewById(R.id.sample_text);
    //tv.setText(stringFromJNI());
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
  //  public native String stringFromJNI();

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }
}
