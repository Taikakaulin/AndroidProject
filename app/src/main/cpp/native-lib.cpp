#include <jni.h>
#include <string>
#include <SLES/OpenSLES_Android.h>
#include <aaudio/AAudio.h>
#include <vector>

/*
extern "C"
JNIEXPORT jstring JNICALL
Java_com_example_k2972_ndk_MainActivity_stringFromJNI(
        JNIEnv* env,
        jobject  this ) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}*/
extern "C"
JNIEXPORT void JNICALL
Java_com_example_k2972_ndk_AlgorythmActivity_RunCpp( JNIEnv* env, jobject, jintArray arr) {

    jsize len = env->GetArrayLength( arr);
    int i, sum = 0;
    jint *body = env->GetIntArrayElements( arr, 0);


    // BUBBLESORT
   for (size_t i = 0; i < len-1; ++i) {
        for (size_t j = 0; j < len-1 - 1 - i; ++j) {
            if (body[j] > body[j + 1]) {
                std::swap(body[j], body[j + 1]);
            }
        }
    }



    env->ReleaseIntArrayElements( arr, body, 0);

}
/*

extern "C"
JNIEXPORT void JNICALL
Java_com_example_k2972_ndk_AudioActivity_StartAudio( JNIEnv* env, jobject, jintArray input this ) {
    // Start audio


    AAudioStreamBuilder* builder = nullptr;
    aaudio_result_t  result = AAudio_createStreamBuilder(&builder);

    if(builder != nullptr) {
        AAudioStream* stream = nullptr;
        result = AAudioStreamBuilder_openStream(builder, &stream);

        if(result == AAUDIO_OK && stream != nullptr) {
            // TODO: ??
        }

        AAudioStreamBuilder_delete(builder);
    }

}

extern "C"
JNIEXPORT void JNICALL
Java_com_example_k2972_ndk_AudioActivity_StopAudio( JNIEnv* env, jobject, jintArray input this ) {
    // Start audio
}*/
